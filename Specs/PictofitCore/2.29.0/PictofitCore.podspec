Pod::Spec.new do |s|
    s.name             = 'PictofitCore'
    s.version          = '2.29.0'
    s.authors          = 'Reactive Reality AG'
    s.license          = { :file => 'PictofitCore.xcframework/LICENSE.md' }
    s.homepage         = 'www.reactivereality.com'
    s.source           = { :http => 'https://gitlab.com/api/v4/projects/19697982/packages/generic/pictofitcore/2.29.0/PictofitCore.xcframework.zip'}
    s.summary          = 'PictofitCore iOS SDK'
    s.description      = <<-DESC
            The PictofitCore SDK for iOS platforms.
    DESC
    s.ios.deployment_target = '11.0'
    s.vendored_frameworks = 'PictofitCore.xcframework'
end
