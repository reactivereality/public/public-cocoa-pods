
# License Terms - SDK
## 1. SUBJECT MATTER OF LICENSE TERMS

1.1 The subject matter of these License Terms is the licensing of Reactive Reality’s Software Development Kit (“**SDK**”) to the Licensee and its affiliates.

1.2 For the avoidance of doubt and not limited to, the following, save to the extent expressly stipulated otherwise in these License Terms, shall not be subject matter of these License Terms:

a. installation or configuration of the SDK;

b. adjustments or customizations of the SDK;

c. integration of the SDK into a Platform;

d. extensions of the SDK;

e. fixing of defects, errors or bugs of the SDK;

f. hosting of any user-facing web services;

g. provision of the source code of the SDK; or

h. technical support, maintenance and/or updates related to the SDK.

## 2. DELIVERY OF SDK

2.1 Reactive Reality makes available the SDK for download by the Licensee from Reactive Reality’s customer online platform "as-is" after the Licensee has registered on Reactive Reality’s website. The SDK shall be deemed delivered upon provision of the link to the customer online platform and the login credentials by Reactive Reality to Licensee.

## 3. REGISTRATION

3.1 Before Reactive Reality makes the SDK available, the Licensee must register via form on Reactive Reality’s website ([Pictofit Website](https://www.pictofit.com)) and provide the following personal information:

If the licensee is a  **natural person**:

· First name and surname

· Date of birth

· E-Mail address

If the licensee is a  **legal person**:

· Full name of the entity

· First name, surname and job title of a contact person

· Address

· E-Mail address

3.2 Once the form has been filled in completely, the "I agree" button will be visible to the Licensee in the form of a checkbox. This button refers to these License Terms, which can be found under the link next to the checkbox.

3.3 Only when the Licensee clicks on the “I agree” button, confirming that he has read and agrees to these License Terms, the link for downloading the SDK will appear. Once the Licensee opens this link, the SDK is downloaded to the device of the Licensee.

## 4. LICENSE AND SCOPE OF USE

4.1 Limited to these License Terms, Reactive Reality grants to the Licensee a worldwide, non-exclusive license for the use of the SDK solely for testing purposes (“**License**”). For the purposes of these Licensing Terms, "testing purposes" means the use of the SDK for company or group internal investigation of whether Reactive Reality's software "Pictofit", an augmented reality/virtual reality rendering service, is suitable for Licensee's IT environment and what the technical requirements for implementation and integration would be. Licensee may not and must not use the SDK for any other purpose, whether commercial or non-commercial, save to the extent expressly stipulated otherwise in these License Terms.

4.2 Licensee may not and must not purport to sub-license, transfer, assign or otherwise dispose of the License granted to it under these License Terms without the prior written consent of Reactive Reality.

4.3 Save to the extent expressly permitted by these License Terms or required by applicable law on a non-excludable basis, Licensee may not and must not permit or engage any third party to

a. alter, edit or adapt the SDK, except as permitted by the inherent capabilities of the SDK, and then only as necessary for the customization of functionality as intended by Reactive Reality;

b. alter, edit or adapt the documentation of the SDK;

c. decompile, reverse engineer or translate, or attempt to decompile, reverse engineer or translate the SDK or otherwise gain access to the source code, structure or proprietary algorithms of the SDK;

d. copy the SDK or parts thereof, except as necessary to use the SDK in accordance with the purpose of integrating or deploying it or to create permitted backup copies of the SDK;

e. sell, resell, rent, lease, loan, supply, publish, distribute or redistribute the SDK or use the SDK for the benefit of any other party but Licensee and its affiliates;

f. modify or delete any copyright or proprietary notices or legends on or in the SDK.

## 5. LICENSE FEE

5.1 The delivery of the SDK and its use are free of charge to the extent the SDK is used in accordance with the scope of these License Terms.

## 6. MAINTENANCE, TECHNICAL SUPPORT AND UPDATES

6.1 Reactive Reality is not obliged under these License Terms to provide to Licensee

a. maintenance services or technical support in relation to the SDK; or

b. updates in relation to the SDK.

## 7. WARRANTY

7.1 Reactive Reality does not warrant the functioning of the SDK or the accuracy of the content available in the SDK.

7.2 Reactive Reality is in any case not responsible for the functioning of the Licensee´s hardware, i.e. the compatibility of the SDK with the hardware used by the Licensee.  Reactive Reality does not warrant for any hardware and software outside its sphere.

## 8. LIABILITY

8.1 Except for intent or severe gross negligence (Vorsatz oder krass grobe  Fahrlässigkeit), to the fullest extent permitted by law, in no event shall Reactive Reality or its directors, employees, agents or contractors be liable to Licensee and its affiliates for any

(i) loss of revenue or income,

(ii) loss of profit or anticipated savings,

(iii) loss of use or production,

(iv) loss of business, contracts or opportunities,

(v) loss of goodwill,

(vi) loss or corruption of any data, database or software, or

(vii) other indirect, special or consequential loss or damage arising out of or in connection with these License Terms, whether in contract, in tort (including negligence), for breach of statutory duty or otherwise;

8.2 The limitation period for any and all liability claims against Reactive Reality shall be 12 months in any case.

8.3 Nothing in these License Terms shall have the effect of excluding or limiting Reactive Reality’s liability for fraud or for death or personal injury caused by its own negligence, or any other liability if and to the extent that the same may not be excluded or limited as a matter of law.

## 9. INTELLECTUAL PROPERTY

9.1 Nothing in these License Terms shall operate to assign or transfer any Intellectual Property Rights from Reactive Reality to Licensee, or from Licensee to Reactive Reality.

9.2 Reactive Reality, in particular but not limited to, remains the owner of all rights, title and interest in all of its technology, including the SDK and in all proprietary file formats.

9.3 Licensee, in particular but not limited to, remains the owner of all rights, title and interest in the output created by using the SDK.

## 10. CONFIDENTIALITY

10.1 Each of the Parties understands and acknowledges that, whether in the course of performance of this License Terms or otherwise, it will receive or become aware of Confidential Information of the other Party. “Confidential Information” means, in relation to either Party and its affiliates, any information, whether in written, electronic, oral or any other form, belonging or relating to that Party or its affiliates, its or their business, affairs, activities, products or services, which information is proprietary to the disclosing Party or its Affiliates, and irrespective of whether such information is marked as “confidential” or not.

10.2 Each of the Parties undertakes to maintain and procure the maintenance of the confidentially of the other Party’s Confidential Information at all times and to keep and procure the keeping of all Confidential Information belonging to the other Party secure and protected against theft, damage, loss, or unauthorised access, and not at any time, whether during the term of this Agreement or at any time thereafter, without the prior written consent of the other Party, directly or indirectly, to use or authorise or permit the use of any of the other Party’s Confidential Information other than as necessary for the sole purpose of the performance of its rights and obligations under this Agreement, or to disclose, exploit, copy or modify any of the other Party’s Confidential Information, or authorise, permit or engage any third party to do the same.

10.3 Each of the Parties undertakes to disclose the other Party’s Confidential Information only to those of its employees, agents and contractors to whom, and to the extent which, such disclosure is necessary for the purposes contemplated under these License Terms, and to procure that such employees, agents and contractors are made aware of and agree in writing to observe the obligations contained in this Section 10.

10.4 Each Party shall immediately upon becoming aware of the same give notice to the other Party of any unauthorised disclosure, misuse, theft or other loss of Confidential Information of the other Party, whether inadvertent or otherwise.

10.5 Each Party shall indemnify the other Party from and against any and all loss or damage incurred by the other Party as a result of any breach by the indemnifying Party or its employees, agents or contractors, of any of its or their obligations under this Section 10.

10.6 The obligations imposed by this Section 10 shall survive the termination of these License Terms.

## 11. PERSONAL DATA

11.1 The Parties may exchange personal data (i.e. names, surnames and contact information) including personal data of their employees (“**Data Subjects**”) as necessary to ensure the performance of these License Terms. The foregoing personal data of the Data Subjects (“**Personal Data**”) shall be processed by the Parties pursuant to the provisions of the General Data Protection Regulation and any applicable national data protection laws (“**GDPR**”).

11.2 Each Party ensures that its processing of Personal Data is lawful under the GDPR, including that it has acquired necessary consents from the Data Subjects and has notices in place to ensure lawful processing and transfer of Personal Data to the other Party.

11.3 Neither Party is obliged to disclose to the other Party any Personal Data if such disclosure is prohibited under the GDPR.

11.4 Information on data protection and on the personal data processed by Reactive Reality or the licensee can be found in the privacy policy, which is an integral part of this contractual relationship between the parties.

## 12. TERM AND TERMINATION

12.1 These License Terms are effective when Licensee downloads, installs, accesses and/or uses the SDK and is valid for an indefinite period of time.

12.2 Licensee may terminate the contract at any time. Deleting the account on Reactive Reality’s website will terminate the contractual relationship of the parties.

12.3 Reactive Reality can cancel the contract at any time without giving reasons by giving 2 weeks-notice to the end of the month. The notification to the Licensee about the termination of the provision of the SDK is considered as termination.

12.4 After expiry of the cancellation period, Licensee no longer has access to the SDK.

12.5 Both parties have the right to terminate the contract with immediate effect for good cause. Reactive Reality is, among other things, entitled to terminate the contract for good cause if the Licensee violates these License Terms or parts thereof.

## 13. COMMUNICATION AND CONTACT

13.1 Reactive Reality can send legally effective messages to the Licensee to the email address provided during his registration on Reactive Reality’s website. Licensee is obliged to inform Reactive Reality immediately of any changes to his email address (given during registration).

13.2 Licensee can send Reactive Reality legally effective messages by e-mail or by post to the following address or e-mail address:

**Reactive Reality AG**

Waltendorfer Hauptstrasse 32a

8010 Graz

Austria

E-Mail: office@reactivereality.com

## 14. ELECTRONIC ADVERTISEMENT

14.1 Reactive Reality sends the user electronic mail by e-mail, SMS, messenger about similar products or services (Art. 107 Par. 3 TKG 2003 or its successor provision).

14.2 The Licensee can object to the sending at any time by sending an email to Reactive Reality with the subject "unsubscribe". Reactive Reality will also give the Licensee the possibility in every message to refuse to receive further messages.

## 15. MISCELLANEOUS

15.1 **Marketing Materials.** Licensee grants Reactive Reality the right to use Licensee’s and/or Licensee’s Platforms’ name and logo on Reactive Reality’s marketing materials (websites, social media channels, e-mail newsletters, print brochures, print catalogues, etc.) solely to indicate that Licensee is a customer of Reactive Reality. Any other use of Licensee’s and/or Licensee’s Platforms’ name and logo in marketing materials of Reactive Reality is only permitted upon prior written consent of Licensee.

15.2 **Confidentiality of these License Terms.** These License Terms are confidential and, with the exception of its employees and professional advisers as well as for the purpose of a due diligence in the course of an investment or transaction, may not be disclosed by either Party to any third party without the other Party’s prior written consent.

15.3 **Notices.** Except as expressly stated herein to the contrary, all notices and other communications required or performed to be given under these License Terms shall be in writing and shall be delivered or transmitted to the other Party’s address as specified above or such other address, including e-mail address, as either Party may notify to the other Party for this purpose from time to time.

15.4 **Assignment.** Licensee may not assign or otherwise transfer or dispose of this Agreement or any of its rights or obligations under this Agreement without the prior written consent of Reactive Reality. Reactive Reality may assign or otherwise transfer this Agreement in whole or in part to any of its affiliates or to any other third party without recourse to Licensee.

15.5 **Change of Ownership or Control.** Licensee shall notify Reactive Reality in writing of any material change of ownership or change of control within 14 days of any such change.

15.6 **Waiver.** The failure of either Party to enforce or exercise, at any time or for any period of time, any term of or any right arising pursuant to this Agreement does not constitute, and shall not be construed as, a waiver of such term or right and shall in no way affect that Party’s right later to enforce or to exercise it.

15.7 **Applicable Law.** These License Terms are governed by, construed and interpreted in accordance with the laws of Austria, without regard to any conflict of laws rules or the United Nations Convention on the International Sale of Goods.

15.8 **Jurisdiction.** With the exception of applications for injunctive relief, any and all disputes or claims arising out of or in connection with these License Terms, including disputes relating to its validity, breach, termination or nullity, shall be finally settled under the Rules of Arbitration (Vienna Rules) of the Vienna International Arbitral Centre (VIAC) of the Austrian  Federal Economic Chamber in accordance with the said rules as follows:

a. The number of arbitrators shall be one.

b. The language to be used in the arbitral proceedings shall be English.

c. The provisions on expedited proceedings (Article 45 Vienna Rules) shall apply.

15.9 **Severability.** If any provision or term of these License Terms is held or rendered illegal, invalid or unenforceable under any applicable law, such provision or term shall, insofar as it is severable from the remaining provisions or terms, be deemed omitted from these License Terms and shall not adversely affect the remaining provisions or terms. In such event, the parties shall use their best endeavors to replace any such illegal, invalid or unenforceable provision or term with provisions or terms which most closely reflect their commercial intent and effect.

15.10 **Entire Agreement.** These License Terms contain all terms agreed between the parties regarding the subject matter and supersedes any prior agreement, understanding or arrangement between the parties, whether oral or in writing. No representation, undertaking or promise shall be taken to have been given or be implied from anything said or written in negotiations between the parties prior to these License Terms.

15.11 **Written Form Requirement.** Any amendments of or additions to these License Terms shall be made in writing and signed by authorized representatives of both Parties to be effective. The same shall also apply to amendments of or additions to this written form requirement.

15.12 **Place of Performance.** The place of performance for all services of Reactive 
Reality under these License Terms is Graz, Austria.
